# pip3 install -r requirements.txt

from __future__ import print_function

import os.path
import pickle
import pandas as pd
import datetime
import numpy as np
import random

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build


def convert_xls_datetime(xls_date):
    if xls_date is None:
        return None
    return (datetime.datetime(1899, 12, 30)
            + datetime.timedelta(days=xls_date))


def _safe_get(data, r, c):
    try:
        value = data[r][c]
        if value == '' or value is None:
            return np.nan
        else:
            return value
    except IndexError:
        return np.nan


def read(range_name, service, spreadsheet_id):
    result = service.spreadsheets().values().get(spreadsheetId=spreadsheet_id,
                                                 range=range_name,
                                                 valueRenderOption='UNFORMATTED_VALUE').execute()
    return result.get('values', [])


def safe_read(sheet, row, col, to_row='', to_col='', service=None, spreadsheet_id=None):
    range_name = '%s!%s%i:%s%s' % (sheet, col, row, to_col, to_row)
    data = read(range_name, service, spreadsheet_id)
    if to_col == '':
        cols = max(len(line) for line in data)
    else:
        if len(to_col) == 1:
            cols = ord(to_col.lower()) - ord(col.lower()) + 1
        else:
            cols = 1
            for i in range(len(to_col)):  # cols('BZ') = 2*27**1 + 27*27**0 because A-Z, AA-AZ + BA-BZ
                cols += (ord(to_col[i].lower()) - ord('a') + 1) * (ord('z') - ord('a') + 1) ** (len(to_col) - i - 1)

    if to_row == '':
        rows = len(data)
    else:
        rows = to_row - row + 1
    return [[_safe_get(data, r, c) for c in range(cols)] for r in range(rows)]


def get_service():
    # If modifying these scopes, delete the file token.pickle.
    scopes = ['https://www.googleapis.com/auth/spreadsheets']
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_id.json', scopes)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)
    return service


def get_teilnehmer_info(service):
    data = safe_read(sheet='Teilnehmer', row=2, col='A', to_row='', to_col='F', service=service,
                     spreadsheet_id='1mwR5rWf2FpPLP7nhO8-xm1H3zbhn6ElHg2jdcoBO9fg')
    df = pd.DataFrame(data)
    df.columns = ['name', 'pair_cooking_only', 'preferred_partner', 'avoid_partner', 'no_max_cooking', 'no_eating_est']
    # replace in pair_cooking_only nas with FALSE, all else with TRUE
    df['pair_cooking_only'] = df['pair_cooking_only'].apply(lambda x: False if pd.isnull(x) else True)
    # replace empty preferred_partner with ""
    df['preferred_partner'] = df['preferred_partner'].apply(lambda x: "" if pd.isnull(x) else x)
    # replace empty avoid_partner with ""
    df['avoid_partner'] = df['avoid_partner'].apply(lambda x: "" if pd.isnull(x) else x)
    # replace no_cooking empty values with 3
    df['no_max_cooking'] = df['no_max_cooking'].apply(lambda x: 2 if np.isnan(x) else x)
    # replace no_eating_est empty values with 8 or estimate from history
    df['no_eating_est'] = df['no_eating_est'].apply(lambda x: 8 if np.isnan(x) else x)
    return df


# returns DataFrame with columns: Tag, Datum, <Names> where in each of the <Names> columns a 0 indicates no time and
# a 1 indicates having time to cook
def get_timeplan(service):
    data = safe_read(sheet='Planung', row=2, col='A', to_row='', to_col='AZ', service=service,
                     spreadsheet_id='1mwR5rWf2FpPLP7nhO8-xm1H3zbhn6ElHg2jdcoBO9fg')  # important to read line 2 for widdth of table
    # old spreadsheet_id: 1ed__J-zGPRXJ2rDF0aiHNVBUHPBOAubPZ5SxYjFUXpA
    df = pd.DataFrame(data)
    # df.columns = safe_read(sheet='Planung', row=2, col='A', to_row=2, to_col='AZ', service=service,
    #                        spreadsheet_id='1mwR5rWf2FpPLP7nhO8-xm1H3zbhn6ElHg2jdcoBO9fg')
    df.columns = data[0]
    df = df.drop(df.index[0])  # delete row with names in it
    df = df.dropna(axis=0, how='all')  # delete all empty rows
    df = df[df.columns.dropna()] # delete all empty columns (no name associated)
    df['Datum'] = df['Datum'].apply(lambda x: convert_xls_datetime(x))

    def clean_function(x):
        # replace all "x" with 0, all other values with 1:
        try:
            if x in ["x", "X"]:
                return 0
            else:
                return 1
        except TypeError:
            return 1

    df = df.apply(lambda x: np.vectorize(clean_function)(x) if x.name not in ['Tag', 'Datum'] else x)
    return df


def get_history(service):
    # TODO: calculate last cooking date
    data = safe_read(sheet='Sheet1', row=1, col='A', to_row='', to_col='AZ', service=service,
                     spreadsheet_id='1aEUunf0hfxBIVCPZD6ybdHAqSFcgRJIDIROhsTTxJ_U')  # important to read line 2 for widdth of table

    df = pd.DataFrame(data)
    df.columns = data[0]
    # df.columns = safe_read(sheet='Sheet1', row=1, col='A', to_row=1, to_col='AZ', service=service,
    #                        spreadsheet_id='1aEUunf0hfxBIVCPZD6ybdHAqSFcgRJIDIROhsTTxJ_U')
    df = df.drop(df.index[0])  # delete row with names in it
    df = df.dropna(axis=0, how='all')  # delete all empty rows
    df = df[df.columns.dropna()]  # delete all empty columns (no name associated)
    del df['Gericht']
    df['Datum'] = df['Datum'].apply(lambda x: convert_xls_datetime(x))

    def clean_function(x):
        # replace all "x" with 0, all other values with 1:
        try:
            if pd.isnull(x):
                return 0
            else:
                return 1
        except TypeError:
            return 1

    df = df.apply(
        lambda x: np.vectorize(clean_function)(x) if x.name not in ['Tag', 'Datum', 'Koch1', 'Koch2', 'Gericht'] else x)
    return df


def calc_no_cooking_past(name, df_history):
    no_cooking = sum(name == df_history['Koch1']) + sum(name == df_history['Koch2'])
    return no_cooking


def calc_meals_cooked(name, df_history):
    no_meals = df_history[name == df_history['Koch1']].drop(['Tag', 'Datum', 'Koch1', 'Koch2'], axis=1).sum().sum()
    no_meals += df_history[name == df_history['Koch2']].drop(['Tag', 'Datum', 'Koch1', 'Koch2'], axis=1).sum().sum()
    return no_meals


def calc_meals_eaten(name, df_history):
    try:
        no_meals_eaten = df_history[~pd.isnull(df_history['Koch1'])][name].sum()
        no_meals_eaten += df_history[~pd.isnull(df_history['Koch2'])][name].sum()
    except KeyError:
        no_meals_eaten = 0
        #print('Could not find ' + name + ' in history table.\n')
    return no_meals_eaten


def calc_score_cooks(df_tn, df_tp):
    avg_meals_per_day = 1 + np.mean(df_tp['Koch2'] != '')  # between 1 and 2, depends on how often pairs are cooking
    avg_meals_eaten_per_day = avg_meals_per_day * df_tn['no_eating_est'].sum() / df_tp.shape[0]
    score_past = df_tn['no_eating_est'] * avg_meals_per_day + df_tn['no_meals_eaten'] - df_tn['no_meals_cooked']
    score_future_cooking = [avg_meals_eaten_per_day * (sum(df_tp['Koch1'] == name) + sum(df_tp['Koch2'] == name))
                            for name in df_tn['name']]
    return score_past - score_future_cooking


def make_random_plan(df_teilnehmer, df_timeplan, min_days_between_cooking=3):
    df_tp = df_timeplan.copy()
    df_tp['cook_needed'] = True
    df_tn = df_teilnehmer.copy()
    df_tp.insert(loc=2, column='Koch2', value='')
    df_tp.insert(loc=2, column='Koch1', value='')

    df_tn['score'] = calc_score_cooks(df_tn, df_tp)
    df_tn = df_tn.sort_values(by='score', axis=0, ascending=False).reset_index(drop=True)
    iterations = 0
    messages = ''
    while any(df_tp['cook_needed']) and iterations < 1000 and len(df_tn["name"])>0:  # df_tn['score'][0] >= 0 and
        iterations += 1  # just to make sure to not get stuck in a loop when there is no solution
        next_cook = df_tn['name'][0]
        messages += "In iteration " + str(iterations) + " cook chosen: " + str(next_cook) + "\n"
        if df_tn['no_max_cooking'][0] <= 0:
            messages += 'Potential Unfairness: ' + str(next_cook) + 'wont be chosen due to restriction how often to ' \
                                                                    'cook per month.\n'
            df_tn = df_tn.drop(df_tn.index[0]).reset_index(drop=True)
            continue
        if sum(df_tp[next_cook] == 1) <= 0:
            messages += 'Potential Unfairness: ' + str(next_cook) + "didn't provide any date for the next period to " \
                                                                    "cook.\n"
            df_tn = df_tn.drop(df_tn.index[0]).reset_index(drop=True)
            continue

        only_pair_cooking = df_tn['pair_cooking_only'][0]
        suggested_dates = df_tp[(df_tp['cook_needed']) & (df_tp[next_cook] == 1)]['Datum'].reset_index(drop=True)

        # if only_pair_cooking:
        #     # adaptation possible: right now, people who want to cook in pairs are always matched
        #     # with people who don't have that need. With a simple | Koch1 == '' this could be fixed
        #     suggested_dates = df_tp[((df_tp['Koch2'] == '') | (df_tp['Koch1'] == '')) & (df_tp[next_cook] == 1)]['Datum'].reset_index(drop=True)
        # else:
        #     suggested_dates = df_tp[(df_tp['Koch1'] == '') & (df_tp[next_cook] == 1)]['Datum'].reset_index(drop=True)
        if suggested_dates.__len__() >= 1:
            # normal case, we found a date for the cook to cook.
            # if possible, try to find a date which has at least min_days_between_cooking days in between cooking days
            # min_days_between_cooking = 0 means you could cook on the next day.
            other_cooking_dates = df_tp[(df_tp['Koch1'] == next_cook) | (df_tp['Koch2'] == next_cook)]['Datum']
            suggested_dates_td = suggested_dates.copy()
            for dt in other_cooking_dates:
                suggested_dates_td = suggested_dates_td[(suggested_dates_td <
                                                         dt + datetime.timedelta((-1) * min_days_between_cooking)) |
                                                        (suggested_dates_td >
                                                         dt + datetime.timedelta(min_days_between_cooking))]
                suggested_dates_td = suggested_dates_td.reset_index(drop=True)
            if suggested_dates_td.__len__() >= 1:
                suggested_dates = suggested_dates_td
            chosen_date = random.choice(suggested_dates)
            messages += "Cook: " + str(next_cook) + " chosen for " + chosen_date.strftime("%Y-%m-%d") + "\n"
            # if there is no Koch1 filled, fill it (regardless of pair-cooking preferences).
            if df_tp[df_tp['Datum'] == chosen_date]['Koch1'].reset_index(drop=True)[0] == "":
                df_tp.loc[df_tp['Datum'] == chosen_date, 'Koch1'] = next_cook
                if not only_pair_cooking:
                    df_tp.loc[df_tp['Datum'] == chosen_date, 'cook_needed'] = False
            else:
                df_tp.loc[df_tp['Datum'] == chosen_date, 'Koch2'] = next_cook
                df_tp.loc[df_tp['Datum'] == chosen_date, 'cook_needed'] = False
            df_tn.loc[0, 'no_max_cooking'] -= 1
        else:
            # there are dates empty, but the cook can't at that time. Switch with a random other(!) cook
            suggested_dates = df_tp[(df_tp['Koch1'] != next_cook) &
                                    (df_tp['Koch2'] != next_cook) &
                                    (df_tp[next_cook] == 1)]['Datum'].reset_index(drop=True)
            if suggested_dates.__len__() < 1:
                messages += 'Potential Unfairness: ' + next_cook + "didn't provide enough dates for the next period " \
                                                                   "to cook.\n"
                df_tn = df_tn.drop(df_tn.index[0]).reset_index(drop=True)
                continue
            chosen_date = random.choice(suggested_dates)
            if only_pair_cooking:
                old_cook = df_tp[df_tp['Datum'] == chosen_date]['Koch2'].tolist()[0]
                df_tp.loc[df_tp['Datum'] == chosen_date, 'Koch2'] = next_cook
            else:
                old_cook = df_tp[df_tp['Datum'] == chosen_date]['Koch1'].tolist()[0]
                df_tp.loc[df_tp['Datum'] == chosen_date, 'Koch1'] = next_cook
            messages += "Cook: " + str(next_cook) + " replaced " + str(old_cook) + " on " + \
                        chosen_date.strftime("%Y-%m-%d") + "\n"
            df_tn.loc[0, 'no_max_cooking'] -= 1
            df_tn.loc[df_tn['name'] == old_cook, 'no_max_cooking'] += 1
        df_tn['score'] = calc_score_cooks(df_tn, df_tp)
        df_tn = df_tn.sort_values(by='score', axis=0, ascending=False).reset_index(drop=True)
    return df_tn, df_tp, messages


def min_days_difference(dates, default_value=14):
    if dates.__len__() < 2:
        return default_value
    dates = dates.sort_values()
    min_diff = dates.iloc[1] - dates.iloc[0]
    for i in range(1, dates.__len__()):
        diff = dates.iloc[i] - dates.iloc[i - 1]
        if diff < min_diff:
            min_diff = diff

    return min_diff.days


def is_partner_preferred(cook1, cook2, df_tn):
    # checks if cook2 is a preferred partner of cook1
    if cook1 =="":
        return(True)
    preferred_partner = df_tn[df_tn['name'] == cook1]['preferred_partner'].tolist()[0].replace(" ", "").split(",")
    if cook2 == "":
        cook2 = "alleine"
    return cook2 in preferred_partner


def is_partner_avoided(cook1, cook2, df_tn):
    # checks if cook2 is not an avoided partner of cook1
    if cook1 == "":
        return(False)
    avoided_partner = df_tn[df_tn['name'] == cook1]['avoid_partner'].tolist()[0].replace(" ", "").split(",")
    if cook2 == "":
        cook2 = "alleine"
    return cook2 in avoided_partner


def is_pair_cooking_honored(cook1, cook2, df_tn):
    # checks if cook2 is a preferred partner of cook1
    only_pair_cooking = df_tn[df_tn['name'] == cook1]['pair_cooking_only'].tolist()
    return not (only_pair_cooking and cook2 == "")


def calc_score_plan(df_tn, df_tp):
    score = 0
    score -= sum(df_tp['Koch1'] == '') * 100  # a day without a cook has 100 negative points

    # check the minimum time difference of days between cooking for each cook. The more days in between
    # cooking the better. np.log2 is used on the number of days. E.g. 8 days in between cooking means a score of 3
    # TODO: look also into history
    cooks = pd.unique(df_tp[['Koch1', 'Koch2']].values.ravel('K'))
    for cook in cooks:
        # print("processing " + str(cook))
        if cook == "":
            continue
        cooking_dates = df_tp[(df_tp['Koch1'] == cook) | (df_tp['Koch2'] == cook)]['Datum']
        min_diff = min_days_difference(cooking_dates)
        score += np.log2(min_diff)

    # scores for each cooking day: is it a preferred partner? is it an avoided partner?
    for index, row in df_tp.iterrows():
        # cooks with preferred partner +10 (per cook)
        # "alleine" == ""
        # print("Processing index " + str(index))
        if is_partner_preferred(row['Koch1'], row['Koch2'], df_tn):
            score += 10
        if row['Koch2'] != "" and is_partner_preferred(row['Koch2'], row['Koch1'], df_tn):
            score += 10
        # cooks with an incompatible partner -20
        if is_partner_avoided(row['Koch1'], row['Koch2'], df_tn):
            # print("partner avoided: " + row['Koch1'] + row['Koch2'])
            score -= 20
        if row['Koch2'] != "" and is_partner_avoided(row['Koch2'], row['Koch1'], df_tn):
            # print("partner avoided: " + row['Koch1'] + row['Koch2'])
            score -= 20
        # checks if people who wants to cook in pairs can do so, else -100
        if not is_pair_cooking_honored(row['Koch1'], row['Koch2'], df_tn):
            # print("pair cooking avoided: " + row['Koch1'] + row['Koch2'])
            score -= 100
        if row['Koch2'] != "" and not is_pair_cooking_honored(row['Koch2'], row['Koch1'], df_tn):
            # print("pair cooking avoided: " + row['Koch1'] + row['Koch2'])
            score -= 100
        if row['Koch1'] == "":
            #not cooking at all
            score -= 100


    # number of cooks in total -10 per cook (?)

    # number of "Potential Unfairness" in messages * (-10) (?)

    return score


def make_plan(df_teilnehmer, df_timeplan, no_iterations=1000):
    highest_score = -10000
    df_tn_max = df_teilnehmer.copy()
    df_tp_max = df_timeplan.copy()
    messages_max = ""
    for i in range(no_iterations):
        print("Makes plan number " + str(i))
        df_tn, df_tp, messages = make_random_plan(df_teilnehmer, df_timeplan,
                                                  min_days_between_cooking=random.choice(range(12)))
        # in df_tn names (rows) are removed if for instance they should cook, but reached the maximum amount already.
        # But these are needed for calc_score_plan to
        df_tn2 = df_teilnehmer.copy()
        df_tn2['score'] = calc_score_cooks(df_tn2, df_tp)
        score_plan = calc_score_plan(df_tn2, df_tp)
        if score_plan > highest_score:
            df_tn_max = df_tn2.copy()
            df_tp_max = df_tp.copy()
            messages_max = messages

    return df_tn_max, df_tp_max, messages_max


def format_plan(df_plan):
    df_plan = df_plan.apply(
        lambda x: '' if x.name not in ['Tag', 'Datum', 'Koch1', 'Koch2', 'Gericht'] else x)
    df_plan['Datum'] = df_plan['Datum'].apply(lambda x: x.strftime("%Y-%m-%d"))
    df_plan['Gast 1'] = ''
    df_plan['Gast 2'] = ''
    df_plan['Gast 3'] = ''
    df_plan['Name'] = ''
    df_plan['Summe'] = ''
    return df_plan


def write_table_to_sheet(df, service, spreadsheet_id, sheet):
    # writes to the spreadsheet_id to sheet From A1 onwards
    sh_range = sheet + "!A1"
    test = service.spreadsheets().values().append(
        spreadsheetId=spreadsheet_id,
        valueInputOption='RAW',
        range=sh_range,
        body=dict(
            majorDimension='ROWS',
            values=df.T.reset_index().T.values.tolist()
        )
    ).execute()
    return test


def main():
    # object which is used to access sheets. It is connected to the google account of user and thus can open all
    # sheets that are shared with or owned by the user of the google account.

    service = get_service()
    df_teilnehmer = get_teilnehmer_info(service)
    df_timeplan = get_timeplan(service)
    df_history = get_history(service)
    df_teilnehmer['no_cooking_past'] = [calc_no_cooking_past(name, df_history) for name in df_teilnehmer['name']]
    df_teilnehmer['no_meals_cooked'] = [calc_meals_cooked(name, df_history) for name in df_teilnehmer['name']]
    df_teilnehmer['no_meals_eaten'] = [calc_meals_eaten(name, df_history) for name in df_teilnehmer['name']]
    plan_teilnehmer, plan_zeit, messages = make_plan(df_teilnehmer, df_timeplan, no_iterations=1000)
    print(messages)
    print("achieved score of " + str(calc_score_plan(df_tn=plan_teilnehmer, df_tp=plan_zeit)))
    df_plan = format_plan(plan_zeit)
    write_table_to_sheet(df_plan, service, spreadsheet_id="1mwR5rWf2FpPLP7nhO8-xm1H3zbhn6ElHg2jdcoBO9fg", sheet="Plan")
    print(plan_zeit)


if __name__ == '__main__':
    main()
