# Kochkreisel

## What is the project about

We use this code for planning a few weeks ahead who should cook when at Grüner Markt. We are a bunch of people with different limitations (some need to cook with others, some dates are not possible for some people).
The primary aim is to make it fair and that people that eat more often also cook more often.

## How to make it run

You will need the file client_id.json. You can get that when you create a Google API via OAuth 2. 
For more information see: https://developers.google.com/sheets/api/guides/authorizing